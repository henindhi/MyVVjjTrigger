#include "SimpleTools.h"
#include <vector>
/*#include "Anti_QCD/EnergyCorrelatorRatiosCalc.h"
#include "Anti_QCD/NSubjettinessRatiosCalc.h"
#include "Anti_QCD/AntiSubjettinessCalc.h"*/

//SimpleTools implementation
SimpleTools::SimpleTools()//:orderBaseValue(NULL)//: FourVecJet(NULL), p4Jet(NULL), SSFloatJet(NULL), SSIntJet(NULL)
{

}

SimpleTools::~SimpleTools()
{

}

void SimpleTools::FourVecRetriever(const xAOD::Jet* jet, std::string& varName, float& FourVecJet)
{
  if (varName == "pt") {
    FourVecJet = jet->pt();
  } else if (varName == "m") {
    FourVecJet = jet->m();
  } else if (varName == "eta") {
    FourVecJet = jet->eta();
  }
  //FourVecJet = jet->varName;
}

void SimpleTools::p4Retriever(const xAOD::Jet* jet, std::string& varName, float& p4Jet)
{
  if (varName == "Et") {
    p4Jet = jet->p4().Et();
  }
  else if (varName == "Eta") {
    p4Jet = jet->p4().Eta();
  }
  else if (varName == "Mass") {
    p4Jet = jet->p4().M();
  }
  //p4Jet = jet->at(jetIndex)->p4.varName;
}

void SimpleTools::SSFloatRetriever(const xAOD::Jet* jet, std::string& varName, float& SSFloatJet)
{
  //SSFloatJet = jet->at(jetIndex)->getAttribute<accessType>(varName);
  SSFloatJet = jet->getAttribute<float>(varName);
}
void SimpleTools::SSIntRetriever(const xAOD::Jet* jet, std::string& varName, int& SSIntJet) {
  SSIntJet = jet->getAttribute<int>(varName);
  //std::cout << "SSIntJet  " << SSIntJet << '\n';
}

void SimpleTools::ValueRetriever(TString& accessType, const xAOD::Jet* jet, std::string& varName, float& floatVar, int& intVar)
{

  //do the dfifferent retriever function
  if (accessType == "4Vec") {
    FourVecRetriever(jet, varName, floatVar);
  }
  else if (accessType == "p4") {
    p4Retriever(jet, varName, floatVar);
  }
  else if (accessType == "float") {
    SSFloatRetriever(jet, varName, floatVar);
  }
  else if (accessType == "int") {
    SSIntRetriever(jet, varName, intVar);
  }

}

void SimpleTools::SortTheJet(const xAOD::Jet* jet, size_t& jetIndex, std::string orderBy, double& orderBaseValue, int& leadJet4VecIndex, int& subLeadJet4VecIndex, double& leadJet, double& subLeadJet, int& passSelCounter)
{
  if (orderBy == "m") {
    orderBaseValue = jet->m();
  } else if (orderBy == "pt") {
    orderBaseValue = jet->pt();
  }

  if (orderBaseValue > subLeadJet) {
    if (orderBaseValue > leadJet) {
      subLeadJet = leadJet;
      subLeadJet4VecIndex = leadJet4VecIndex;
      leadJet = orderBaseValue;
      leadJet4VecIndex = jetIndex;
    }
    else {
      subLeadJet = orderBaseValue;
      subLeadJet4VecIndex = jetIndex;
    }
  }

  passSelCounter++;
  //std::cout << "number of jet that pass selection" << passSelCounter << '\n';
}

void SimpleTools::MatchTheJet(TLorentzVector& jetToMatch_p4, size_t& currIndex, TLorentzVector& targetLdgJet_p4, TLorentzVector& targetSubLdgJet_p4, double& dRmin_LdgJet, double& dRmin_SubLdgJet, int& targetLdgIndex, int& targetSubLdgIndex, int& matchedLdgIndex, int& matchedSubLdgIndex, int& counter)
{
  // Find the distance between jets
  double dR_LdgJet = 0, dR_SubLdgJet = 0;
  if (counter >= 1 && targetLdgIndex != -1) {
    dR_LdgJet = targetLdgJet_p4.DeltaR(jetToMatch_p4);
  }

  if (counter > 1 && targetSubLdgIndex != -1) {
    dR_SubLdgJet = targetSubLdgJet_p4.DeltaR(jetToMatch_p4);
  }

  if (dR_LdgJet < dRmin_LdgJet)
  {
    dRmin_LdgJet =dR_LdgJet;
    matchedLdgIndex = currIndex;
  }
  if (counter > 1 && targetSubLdgIndex != -1 && dR_LdgJet < dRmin_SubLdgJet)
  {
   dRmin_SubLdgJet = dR_SubLdgJet;
   matchedSubLdgIndex = currIndex;
  }
}

void SimpleTools::EtFinder(TH1I*& refHisto, TH1I*& histo, float& EtCut, int& binContent)
{
  float target = 460;
  int binNumber = refHisto->GetXaxis()->FindBin(target);
  binContent = refHisto->GetBinContent(binNumber);
  //std::cout << "bin number : " << binNumber << "| bin content : "  << binContent  << std::endl;

  int rateDiff = 10000;
  EtCut = 0;
  for (int nBin = 0; nBin < histo->GetSize(); ++nBin){
    int currBinContent = histo->GetBinContent(nBin);
    if (abs(binContent - currBinContent) < rateDiff){
      rateDiff = abs(binContent - currBinContent);
      EtCut = histo->GetBinCenter(nBin);

      if (abs(binContent - currBinContent) == 0){
        break;
        }
    }
    else if (abs(binContent - currBinContent) == rateDiff){
      break;
    }
  }

  std::cout << "Histo "<< histo->GetName()  <<  " | Et : "  << EtCut  <<  std::endl;
}

/*void SimpleTools::CalcDerived (std::vector<float>& vec, const xAOD::Jet* jet)
{
  float c1 = 0, c2 = 0, d2 = 0, tau21 = 0, tau32 = 0, tau21_wta = 0, tau32_wta = 0, tau21_3 = 0, tau32_1 = 0, tau21_3_wta = 0, tau32_1_wta = 0;

  HCTriggerBase hcBase("hcBase");
  EnergyCorrelatorRatiosCalc eCorrCalc ("eCorrCalc");
  AntiSubjettinessCalc aSubCalc ("aSubCalc");
  NSubjettinessRatiosCalc nSubCalc ("nSubCalc");

  eCorrCalc.CalculateECFRatio(jet, c1, c2, d2);
  nSubCalc.CalculateNSubRatio(jet, tau21, tau21_wta, tau32, tau32_wta);
  aSubCalc.CalculateAntiSubjet(jet, tau21_3, tau21_3_wta, tau32_1, tau32_1_wta);
  vec.push_back(c1);
  vec.push_back(c2);
  vec.push_back(d2);
  vec.push_back(tau21);
  vec.push_back(tau21_wta);
  vec.push_back(tau32);
  vec.push_back(tau32_wta);
  vec.push_back(tau21_3);
  vec.push_back(tau21_3_wta);
  vec.push_back(tau32_1);
  vec.push_back(tau32_1_wta);
}*/
