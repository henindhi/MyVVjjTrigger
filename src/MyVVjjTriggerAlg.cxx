// MyVVjjTrigger includes
#include "MyVVjjTriggerAlg.h"
#include "SimpleTools.h"
//GoodRunsList
//#include "GoodRunsLists/GoodRunsListSelectionTool.h"

//#include "xAODTruth/TruthParticle.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/EventAuxInfo.h"

//#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODParticleEvent/IParticleLink.h"
#include "xAODPFlow/TrackCaloClusterContainer.h"

//#include "JetCalibTools/JetCalibrationTool.h"

// ROOT includes
#include "TString.h"
#include "TChain.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLine.h"
#include "TNamed.h"

//#include "xAODEventInfo/EventInfo.h"

MyVVjjTriggerAlg::MyVVjjTriggerAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ),
m_jetCalibration()
{
  declareProperty("JetCalibrationTools"         , m_jetCalibration);
  declareProperty("ApplyCalibration"            , m_applyCalibration = true);
  declareProperty("CollectionsToCalibrate"      , m_jetCalibrationCollections);
  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


MyVVjjTriggerAlg::~MyVVjjTriggerAlg() {}


StatusCode MyVVjjTriggerAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  //jet calib
  if (m_applyCalibration) {
    m_jetCalibration.setTypeAndName("JetCalibrationTool/MyJetCalibTool");
    CHECK( m_jetCalibration.retrieve() ); //optional, just forces initializing the tool here instead of at first use
  }

  m_tdt.setTypeAndName("Trig::TrigDecisionTool/TrigDecisionTool");
  CHECK( m_tdt.initialize() );

  m_hJ360_pass = new TH1D("hJ360_pass","hJ360_pass",28,320,600);
  CHECK( histSvc()->regHist("/MYSTREAM/hJ360_pass", m_hJ360_pass) );
  m_hJ390_pass = new TH1D("hJ390_pass","hJ390_pass",28,320,600);
  CHECK( histSvc()->regHist("/MYSTREAM/hJ390_pass", m_hJ390_pass) );
  m_hJ420_pass = new TH1D("hJ420_pass","hJ420_pass",28,320,600);
  CHECK( histSvc()->regHist("/MYSTREAM/hJ420_pass", m_hJ420_pass) );
  m_hJ440_pass = new TH1D("hJ440_pass","hJ440_pass",28,320,600);
  CHECK( histSvc()->regHist("/MYSTREAM/hJ440_pass", m_hJ440_pass) );
  m_hJ460_pass = new TH1D("hJ460_pass","hJ460_pass",28,320,600);
  CHECK( histSvc()->regHist("/MYSTREAM/hJ460_pass", m_hJ460_pass) );

  m_h_total = new TH1D("h_total","h_total",28,320,600);
  CHECK( histSvc()->regHist("/MYSTREAM/h_total", m_h_total) );

  m_passHistos.push_back(m_h_total);
  m_passHistos.push_back(m_hJ360_pass);
  m_passHistos.push_back(m_hJ390_pass);
  m_passHistos.push_back(m_hJ420_pass);
  m_passHistos.push_back(m_hJ440_pass);
  m_passHistos.push_back(m_hJ460_pass);

  m_hJ360mjj_pass = new TH1D("hJ360mjj_pass","hJ360mjj_pass",50,0,2000);
  CHECK( histSvc()->regHist("/MYSTREAM/hJ360mjj_pass", m_hJ360mjj_pass) );
  m_hJ390mjj_pass = new TH1D("hJ390mjj_pass","hJ390mjj_pass",50,0,2000);
  CHECK( histSvc()->regHist("/MYSTREAM/hJ390mjj_pass", m_hJ390mjj_pass) );
  m_hJ420mjj_pass = new TH1D("hJ420mjj_pass","hJ420mjj_pass",50,0,2000);
  CHECK( histSvc()->regHist("/MYSTREAM/hJ420mjj_pass", m_hJ420mjj_pass) );
  m_hJ440mjj_pass = new TH1D("hJ440mjj_pass","hJ440mjj_pass",50,0,2000);
  CHECK( histSvc()->regHist("/MYSTREAM/hJ440mjj_pass", m_hJ440mjj_pass) );
  m_hJ460mjj_pass = new TH1D("hJ460mjj_pass","hJ460mjj_pass",50,0,2000);
  CHECK( histSvc()->regHist("/MYSTREAM/hJ460mjj_pass", m_hJ460mjj_pass) );

  m_h_mjj_total = new TH1D("h_mjj_total","h_mjj_total",50,0,2000);
  CHECK( histSvc()->regHist("/MYSTREAM/h_mjj_total", m_h_mjj_total) );

  m_passMjjHistos.push_back(m_h_mjj_total);
  m_passMjjHistos.push_back(m_hJ360mjj_pass);
  m_passMjjHistos.push_back(m_hJ390mjj_pass);
  m_passMjjHistos.push_back(m_hJ420mjj_pass);
  m_passMjjHistos.push_back(m_hJ440mjj_pass);
  m_passMjjHistos.push_back(m_hJ460mjj_pass);

  triggerNames.push_back("HLT_j360_a10_lcw_sub_L1J100");
  triggerNames.push_back("HLT_j390_a10t_lcw_jes_30smcINF_L1J100");
  triggerNames.push_back("HLT_j420_a10_lcw_L1J100");
  triggerNames.push_back("HLT_j440_a10t_lcw_jes_L1J100");
  triggerNames.push_back("HLT_j460_a10t_lcw_jes_L1J100");

  //2017
  ptCutMjj.push_back(420.e3);//360,430
  ptCutMjj.push_back(440.e3);//390,
  ptCutMjj.push_back(470.e3);//420,470
  ptCutMjj.push_back(490.e3);//440,490
  ptCutMjj.push_back(510.e3);//460,510

  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory


  return StatusCode::SUCCESS;
}

StatusCode MyVVjjTriggerAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode MyVVjjTriggerAlg::execute() {
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  //setFilterPassed(false); //optional: start with algorithm not passed

  //simple tools
  SimpleTools myTools;
  //for offline ordering
  int firstJetIndex = -1, secondJetIndex = -1, selCounter = 0;
  double firstJet = 0, secondJet = 0, currVal = 0;
  std::string jetOrder = "pt";

  //getting event info
  const xAOD::EventInfo* info = nullptr;
  if (evtStore()->retrieve(info).isFailure()){
    ATH_MSG_FATAL( "Unable to retrieve Event Info" );
  }

  for (auto name : m_jetCalibrationCollections) {
    //get the jet container
    const xAOD::JetContainer* jets = nullptr;
    //std::string name = "AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20Jets";
    if (evtStore()->retrieve(jets,name).isFailure())
    {
      ATH_MSG_FATAL("Failed to retrieve trigger jet collection");
    }

    //sort the calibrated jet
    for (size_t iJet = 0; iJet < jets->size(); ++iJet)
    {
      const xAOD::Jet* jet = jets->at(iJet);
      xAOD::Jet* calibJet = nullptr;
      m_jetCalibration->calibratedCopy(*jet,calibJet);
      // Do things with your newly calibrated jet
      // Below is just one example (using std::cout to be generic)
      // std::cout << "Jet (pT,eta,phi,m):" << std::endl;
      // std::cout << "\t Before: (" << jet->pt() << "," << jet->eta() << "," << jet->phi() << "," << jet->m() << ")" << std::endl;
      // std::cout << "\t After : (" << calibJet->pt() << "," << calibJet->eta() << "," << calibJet->phi() << "," << calibJet->m() << ")" << std::endl;
      // When done using the calibrated jet, free the associated memory

      // auto allChains = m_tdt->getChainGroup(".*");
      // std::cout << "Triggers that passed : ";
      // for(auto& trig : allChains->getListOfTriggers()) if(m_tdt->getChainGroup(trig)->isPassed()) std::cout << trig << ", ";
      // std::cout << std::endl;
      // here's how to test a specific trigger:
      // m_tdt->isPassed("MYTRIGGER");
      myTools.SortTheJet(calibJet, iJet, jetOrder, currVal, firstJetIndex, secondJetIndex, firstJet, secondJet, selCounter);
      delete calibJet;
    }

    if (firstJetIndex != -1 && secondJetIndex != -1 && selCounter > 1) {
      for (size_t iHist = 0, count = m_passHistos.size(); iHist < count; iHist++) {
        const xAOD::Jet* leadJet = jets->at(firstJetIndex);
        const xAOD::Jet* subleadJet = jets->at(secondJetIndex);
        xAOD::Jet* calibLeadJet = nullptr;
        xAOD::Jet* calibsubLeadJet = nullptr;
        m_jetCalibration->calibratedCopy(*leadJet,calibLeadJet);
        m_jetCalibration->calibratedCopy(*subleadJet,calibsubLeadJet);

        TLorentzVector ldgJet_p4 = calibLeadJet->p4();
        TLorentzVector subLdgJet_p4 = calibsubLeadJet->p4();

        double y1 = ldgJet_p4.Rapidity();
        double y2 = subLdgJet_p4.Rapidity();
        double delta_y = abs(y1-y2);

        //asymmetry
        double asym = (ldgJet_p4.Pt() - subLdgJet_p4.Pt())/(ldgJet_p4.Pt() + subLdgJet_p4.Pt());
        //mjj
        TLorentzVector leadSubLeadJet_p4 = ldgJet_p4 + subLdgJet_p4;
        double mjj = leadSubLeadJet_p4.M();

        if (delta_y < 1.2 && asym < 0.15 && ldgJet_p4.M() > 50.e3 &&  subLdgJet_p4.M() > 50.e3 && std::abs(ldgJet_p4.Eta()) < 2.0 && std::abs(subLdgJet_p4.Eta()) < 2.0) {
          if (iHist == 0) {
            m_passHistos.at(0)->Fill(ldgJet_p4.Pt()/1.e3);
            m_passMjjHistos.at(0)->Fill(mjj/1.e3);
          } else if (m_tdt->isPassed(triggerNames.at(iHist-1))) {
            //std::cout << "pass trigger : " << triggerNames.at(iHist-1) << '\n';
            m_passHistos.at(iHist)->Fill(ldgJet_p4.Pt()/1.e3);
            if (ldgJet_p4.Pt() > ptCutMjj.at(iHist-1)) {
              m_passMjjHistos.at(iHist)->Fill(mjj/1.e3);
            }
          }
        }
        delete calibLeadJet;
        delete calibsubLeadJet;
      }
    }
  }

  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //


  //HERE IS AN EXAMPLE
  //const xAOD::EventInfo* ei = 0;
  //CHECK( evtStore()->retrieve( ei , "EventInfo" ) );
  //ATH_MSG_INFO("eventNumber=" << ei->eventNumber() );
  //m_myHist->Fill( ei->averageInteractionsPerCrossing() ); //fill mu into histogram


  //setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}

StatusCode MyVVjjTriggerAlg::beginInputFile() {
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}
