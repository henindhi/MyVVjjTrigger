
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../MyVVjjTriggerAlg.h"

DECLARE_ALGORITHM_FACTORY( MyVVjjTriggerAlg )

DECLARE_FACTORY_ENTRIES( MyVVjjTrigger ) 
{
  DECLARE_ALGORITHM( MyVVjjTriggerAlg );
}
