#ifndef MYVVJJTRIGGER_MYVVJJTRIGGERALG_H
#define MYVVJJTRIGGER_MYVVJJTRIGGERALG_H
/**
 *@author Herjuno Nindhito
**/

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
//gaudi
#include "GaudiKernel/ToolHandle.h"
//EDM
//#include "xAODTruth/TruthParticleContainer.h"
//calibration
#include "JetCalibTools/IJetCalibrationTool.h"
#include "AsgTools/AnaToolHandle.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
//GoodRunsLists
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
//standard library
#include <string>
#include <vector>

//ROOT Includes
//#include "TEfficiency.h"
//#include "TTree.h"
//#include "TH1D.h"


class MyVVjjTriggerAlg: public ::AthAnalysisAlgorithm {
 public:
  MyVVjjTriggerAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MyVVjjTriggerAlg();

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed


  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private:

   //Example algorithm property, see constructor for declaration:
   //int m_nProperty = 0;

   //Example histogram, see initialize method for registration to output histSvc
   //TH1D* m_myHist = 0;
   //TTree* m_myTree = 0;
   TH1D* m_hJ360_pass = 0;
   TH1D* m_hJ390_pass = 0;
   TH1D* m_hJ420_pass = 0;
   TH1D* m_hJ440_pass = 0;
   TH1D* m_hJ460_pass = 0;

   TH1D* m_h_total = 0;

   TH1D* m_hJ360mjj_pass = 0;
   TH1D* m_hJ390mjj_pass = 0;
   TH1D* m_hJ420mjj_pass = 0;
   TH1D* m_hJ440mjj_pass = 0;
   TH1D* m_hJ460mjj_pass = 0;

   TH1D* m_h_mjj_total = 0;

   //vector of histograms
   std::vector<TH1D*> m_passHistos;
   std::vector<TH1D*> m_passMjjHistos;
   //vector of trigger name
   std::vector<std::string> triggerNames;
   std::vector<float> ptCutMjj;
   //calibration tool
   ToolHandle<IJetCalibrationTool> m_jetCalibration;
   //jet container
   std::vector<std::string> m_jetCalibrationCollections;
   asg::AnaToolHandle<Trig::TrigDecisionTool> m_tdt;
   //truth jet container
   //std::vector<std::string> m_jetCalibrationTruthCollections;
   //histograms - still not sure

   bool m_applyCalibration;

};

#endif //> !MYVVJJTRIGGER_MYVVJJTRIGGERALG_H
