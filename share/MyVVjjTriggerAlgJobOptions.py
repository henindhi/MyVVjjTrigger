#Skeleton joboption for a simple analysis job

#---- Minimal joboptions -------
import json

theApp.EvtMax=-1                                         #says how many events to run over. Set to -1 for all events

with open("/beegfs/users/henindhi/MyVVjjTrigger/source/inputCollection/data15/x000") as f:
    lines = [line.replace('\n','') for line in f]

#print lines
#json.dumps(lines)
jps.AthenaCommonFlags.FilesInput = lines
#data15
#["root://grid05.unige.ch//dpm/unige.ch/home/atlas/atlaslocalgroupdisk/rucio/data15_13TeV/3a/23/DAOD_EXOT3.12640132._000001.pool.root.1"]
#data16
#["root://grid05.unige.ch//dpm/unige.ch/home/atlas/atlaslocalgroupdisk/rucio/data16_13TeV/41/54/DAOD_EXOT3.12645626._000001.pool.root.1"]
#data17
#["root://grid05.unige.ch//dpm/unige.ch/home/atlas/atlaslocalgroupdisk/rucio/data17_13TeV/7d/6e/DAOD_EXOT3.12638438._000001.pool.root.1"]
#["root://grid05.unige.ch//dpm/unige.ch/home/atlas/atlaslocalgroupdisk/rucio/data17_13TeV/7d/6e/DAOD_EXOT3.12638438._000001.pool.root.1","root://grid05.unige.ch//dpm/unige.ch/home/atlas/atlaslocalgroupdisk/rucio/data17_13TeV/96/be/DAOD_EXOT3.12638438._000002.pool.root.1"]
#insert your list of input files here (do this before next lines)
#["/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CommonInputs/DAOD_PHYSVAL/mc16_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_PHYSVAL.e5458_s3126_r9364_r9315_AthDerivation-21.2.1.0.root"]

#Now choose your read mode (POOL, xAOD, or TTree):

if not hasattr(svcMgr, 'THistSvc'): svcMgr += CfgMgr.THistSvc() #only add the histogram service if not already present (will be the case in this jobo)
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='/atlas/users/henindhi/MyVVjjTrigger/source/outputWithGrl/myfileVVjj15_x000.root' OPT='RECREATE'"] #add an output root file stream

#POOL:
import AthenaPoolCnvSvc.ReadAthenaPool                   #sets up reading of any POOL files (but POOL is slow)

#xAOD:
#import AthenaRootComps.ReadAthenaxAODHybrid               #FAST xAOD reading!

#TTree:
#import AthenaRootComps.ReadAthenaRoot                    #read a flat TTree, very fast, but no EDM objects
#svcMgr.EventSelector.TupleName="MyTree"                  #You usually must specify the name of the tree (default: CollectionTree)

algseq = CfgMgr.AthSequencer("AthAlgSeq")                #gets the main AthSequencer
algseq += CfgMgr.MyVVjjTriggerAlg()                                 #adds an instance of your alg to it

ToolSvc = ServiceMgr.ToolSvc

from MyVVjjTrigger.MyVVjjTriggerConf import MyVVjjTriggerAlg
tool = MyVVjjTriggerAlg()
tool.CollectionsToCalibrate = ["AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20Jets"]
tool.OutputLevel = INFO
print tool
ToolSvc += tool

ToolSvc += CfgMgr.JetCalibrationTool("MyJetCalibTool")
ToolSvc.MyJetCalibTool.JetCollection = "AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20"
ToolSvc.MyJetCalibTool.ConfigFile    = "JES_JMS_AntiKt10TrackCaloClusterTrimmedPtFrac5SmallR20_29Sep17.config" # Enter the calorimeter or track-assisted mass config here
ToolSvc.MyJetCalibTool.CalibSequence = "EtaJES_JMS"
ToolSvc.MyJetCalibTool.DEVmode       = True
ToolSvc.MyJetCalibTool.IsData        = True #if you are running over data, else False

ToolSvc += CfgMgr.GoodRunsListSelectionTool("GRLTool",GoodRunsListVec=["/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20171130/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"])
filterSeq = CfgMgr.AthSequencer("AthFilterSeq")
filterSeq += CfgMgr.GRLSelectorAlg(Tool=ToolSvc.GRLTool)

svcMgr.MetaDataSvc.MetaDataTools += [ "LumiBlockMetaDataTool" ]
theApp.CreateSvc += ['xAOD2NtupLumiSvc']

#not sure what I'm doing here
from AthenaCommon.AppMgr import theApp
ServiceMgr.MessageSvc.OutputLevel = INFO
ServiceMgr.MessageSvc.defaultLimit = 10000
#-------------------------------


#note that if you edit the input files after this point you need to pass the new files to the EventSelector:
#like this: svcMgr.EventSelector.InputCollections = ["new","list"]





##--------------------------------------------------------------------
## This section shows up to set up a HistSvc output stream for outputing histograms and trees
## See https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_output_trees_and_histogra for more details and examples


##--------------------------------------------------------------------


##--------------------------------------------------------------------
## The lines below are an example of how to create an output xAOD
## See https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_create_an_output_xAOD for more details and examples

#from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
#xaodStream = MSMgr.NewPoolRootStream( "StreamXAOD", "xAOD.out.root" )

##EXAMPLE OF BASIC ADDITION OF EVENT AND METADATA ITEMS
##AddItem and AddMetaDataItem methods accept either string or list of strings
#xaodStream.AddItem( ["xAOD::JetContainer#*","xAOD::JetAuxContainer#*"] ) #Keeps all JetContainers (and their aux stores)
#xaodStream.AddMetaDataItem( ["xAOD::TriggerMenuContainer#*","xAOD::TriggerMenuAuxContainer#*"] )
#ToolSvc += CfgMgr.xAODMaker__TriggerMenuMetaDataTool("TriggerMenuMetaDataTool") #MetaDataItems needs their corresponding MetaDataTool
#svcMgr.MetaDataSvc.MetaDataTools += [ ToolSvc.TriggerMenuMetaDataTool ] #Add the tool to the MetaDataSvc to ensure it is loaded

##EXAMPLE OF SLIMMING (keeping parts of the aux store)
#xaodStream.AddItem( ["xAOD::ElectronContainer#Electrons","xAOD::ElectronAuxContainer#ElectronsAux.pt.eta.phi"] ) #example of slimming: only keep pt,eta,phi auxdata of electrons

##EXAMPLE OF SKIMMING (keeping specific events)
#xaodStream.AddAcceptAlgs( "MyVVjjTriggerAlg" ) #will only keep events where 'setFilterPassed(false)' has NOT been called in the given algorithm

##--------------------------------------------------------------------


include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above
